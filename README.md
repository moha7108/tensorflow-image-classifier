# tensorflow image classifier

Image classifier built with tensorflow used to identify 102 flower species with 85 % accuracy. This python application takes command line  arguments  to predict the flower species of a provided image.


This model provided in this example was built from the  mobilenet neural network, https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/, and re-trained on the Oxford Flowers 102 dataset 


This project was developed in fulfillment  of the requirements of the Udacity Machine Learning with Tensorflow Nanodegree Program

### To run use: 

predict.py image_path model_path --top_k --category_names