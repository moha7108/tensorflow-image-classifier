import numpy as np
import tensorflow as tf
from PIL import Image
import json

from get_args import get_args
from load_model import load_model
import engine

parameters = get_args()


with open(parameters.category_names, 'r') as f:
    class_names = json.load(f)


model = load_model(parameters.model_path)

predictions, classes = engine.predict(parameters.img_file, model, parameters.top_k,class_names)

print(f'Predicted Classification: {classes[-1]}')

print(f'Top {parameters.top_k} Choices:===================================================================')

for i in range(0,parameters.top_k):
    
    print(f'Classifier: {classes[i]}, Corresponding Probability: {predictions[i]*100}')