import tensorflow as tf
import numpy as np
from PIL import Image



def process_image(image_to_process):
    
    img = tf.convert_to_tensor(image_to_process)
    
    img = tf.image.resize(img,(224,224))
    img /= 255
    
    return img.numpy()

def predict(image_path, model, top_k, class_names):
    
    img = Image.open(image_path)
    img = np.asarray(img)
    
    img = process_image(img)
    img = np.expand_dims(img, axis=0)
    predictions = model.predict(img).squeeze()
    idx = np.argsort(predictions)
    predictions = predictions[idx]
    
    classes = []
    for key in idx[-top_k:]:
        
        classes.append(class_names[str(key+1)])
    
    return predictions[-top_k:], classes
